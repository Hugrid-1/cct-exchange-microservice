package workers

import (
	"context"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/public/interfaces"
	"go.uber.org/zap"
	"time"
)

const (
	tickerInterval = 10 * time.Second
)

type Worker struct {
	service interfaces.Exchanger
	logger  *zap.Logger
}

func NewWorker(service interfaces.Exchanger, logger *zap.Logger) *Worker {
	return &Worker{service: service, logger: logger}
}

func (r *Worker) Run() {
	r.logger.Info("start ticker parser")

	ctx := context.TODO()

	go func() {
		ticker := time.NewTicker(tickerInterval)
		defer ticker.Stop()

		for {
			select {
			case <-ctx.Done():
				return
			case <-ticker.C:
				r.service.GetTicker(ctx)
				r.logger.Info("Ticker: ticker data received successfully ")
			}
		}
	}()
}
