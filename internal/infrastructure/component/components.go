package component

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/config"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/internal/infrastructure/responder"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/internal/infrastructure/tools/cryptography"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/rabbitmq"
	"go.uber.org/zap"
)

type Components struct {
	Conf         config.AppConf
	TokenManager cryptography.TokenManager
	Responder    responder.Responder
	Decoder      godecoder.Decoder
	Logger       *zap.Logger
	Hash         cryptography.Hasher
	RabbitMQ     rabbitmq.RabbitMQBroker
}

func NewComponents(conf config.AppConf, rabbitMQ rabbitmq.RabbitMQBroker, tokenManager cryptography.TokenManager, responder responder.Responder, decoder godecoder.Decoder, hash cryptography.Hasher, logger *zap.Logger) *Components {
	return &Components{Conf: conf, TokenManager: tokenManager, Responder: responder, Decoder: decoder, Hash: hash, Logger: logger, RabbitMQ: rabbitMQ}
}
