package docs

import (
	"gitlab.com/Hugrid-1/cct-exchange-microservice/public/messages"
)

//go:generate swagger generate spec -o ../../static/swagger.json --scan-models

// swagger:route GET /api/1/exchange/ticker exchange tickerRequest
// Получение средних цен.
// responses:
//   200: tickerResponse

// swagger:response tickerResponse
type tickerResponse struct {
	// in:body
	Body messages.ExchangeOut
}

// swagger:route GET /api/1/exchange/history exchange historyRequest
// Получение истории.
// responses:
//   200: historyResponse

// swagger:response historyResponse
type historyResponse struct {
	// in:body
	Body messages.ExchangeHistoryOut
}

// swagger:route GET /api/1/exchange/min exchange minPairsRequest
// Получение минимальных цен.
// responses:
//   200: minPairsResponse

// swagger:response minPairsResponse
type minPairsResponse struct {
	// in:body
	Body messages.ExchangeOut
}

// swagger:route GET /api/1/exchange/max exchange maxPairsRequest
// Получение максимальных цен.
// responses:
//   200: minPairsResponse

// swagger:response maxPairsResponse
type maxPairsResponse struct {
	// in:body
	Body messages.ExchangeOut
}

// swagger:route GET /api/1/exchange/avg exchange avgPairsRequest
// Получение тикера.
// responses:
//   200: avgPairsResponse

// swagger:response avgPairsResponse
type avgPairsResponse struct {
	// in:body
	Body messages.ExchangeOut
}

// swagger:route GET /api/1/exchange/names exchange namePairsRequest
// Получение тикера.
// responses:
//   200: namePairsResponse

// swagger:response namePairsResponse
type namePairsResponse struct {
	// in:body
	Body messages.ExchangePairsListOut
}
