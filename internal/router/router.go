package router

import (
	"github.com/go-chi/chi/v5"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/internal/infrastructure/component"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/internal/modules"
	"net/http"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()

	r.Route("/api", func(r chi.Router) {
		r.Route("/1", func(r chi.Router) {
			//authCheck := middleware.NewTokenManager(components.Responder, components.TokenManager)

			r.Route("/exchange", func(r chi.Router) {
				exchangeController := controllers.Exchange
				r.Get("/ticker", exchangeController.GetTicker)
				r.Get("/history", exchangeController.GetHistory)
				r.Get("/min", exchangeController.GetMaxPairs)
				r.Get("/max", exchangeController.GetMinPairs)
				r.Get("/avg", exchangeController.GetAvgPairs)
				r.Get("/names", exchangeController.GetPairsNameList)

			})
		})
	})

	return r
}
