package modules

import (
	"gitlab.com/Hugrid-1/cct-exchange-microservice/internal/infrastructure/component"
	exservice "gitlab.com/Hugrid-1/cct-exchange-microservice/internal/modules/exchange/service"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/internal/storages"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/public/interfaces"
)

type Services struct {
	Exchange interfaces.Exchanger
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	exchangeService := exservice.NewExchangeService(storages.Exchange, components.Logger, components.RabbitMQ)
	return &Services{
		Exchange: exchangeService,
	}
}
