package storage

//go:generate easytags $GOFILE json,db,db_ops,db_type,db_default,db_index

// this table contains history
type ExchangeDTO struct {
	ID        int64   `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null" db_ops:"id"`
	PairName  string  `json:"pair_name" db:"pair_name" db_type:"varchar(55)" db_default:"default null" db_ops:"create,update"`
	BuyPrice  float64 `json:"buy_price" db:"buy_price" db_type:"real" db_default:"default null" db_ops:"create,update"`
	SellPrice float64 `json:"sell_price" db:"sell_price" db_type:"real" db_default:"default null" db_ops:"create,update"`
	LastTrade float64 `json:"last_trade" db:"last_trade" db_type:"real" db_default:"default null" db_ops:"create,update"`
	High      float64 `json:"high" db:"high" db_type:"real" db_default:"default null" db_ops:"create,update"`
	Low       float64 `json:"low" db:"low" db_type:"real" db_default:"default null" db_ops:"create,update"`
	Avg       float64 `json:"avg" db:"avg" db_type:"real" db_default:"default null" db_ops:"create,update"`
	Vol       float64 `json:"vol" db:"vol" db_type:"real" db_default:"default null" db_ops:"create,update"`
	VolCurr   float64 `json:"vol_curr" db:"vol_curr" db_type:"real" db_default:"default null" db_ops:"create,update"`
	Updated   int64   `json:"updated" db:"updated" db_type:"integer" db_default:"default null" db_ops:"create,update"`
}

func (ex *ExchangeDTO) TableName() string {
	return "pairs_history"
}

func (ex *ExchangeDTO) OnCreate() []string {
	return []string{}
}

type ExchangeMaxDTO struct {
	ID         int64  `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null" db_ops:"id"`
	PairName   string `json:"pair_name" db:"pair_name" db_type:"varchar(55)" db_default:"default null" db_ops:"create,update"`
	ExchangeID int64  `json:"exchange_id" db:"exchange_id" db_type:"integer" db_default:"default null" db_ops:"create,update"`
}

func (e ExchangeMaxDTO) TableName() string {
	return "max_pairs"
}

func (e ExchangeMaxDTO) OnCreate() []string {
	return []string{}
}

type ExchangeMinDTO struct {
	ID         int64  `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	PairName   string `json:"pair_name" db:"pair_name" db_type:"varchar(55)" db_default:"default null" db_ops:"create,update"`
	ExchangeID int64  `json:"exchange_id" db:"exchange_id" db_type:"integer" db_default:"default null" db_ops:"create,update"`
}

func (e ExchangeMinDTO) TableName() string {
	return "min_pairs"
}

func (e ExchangeMinDTO) OnCreate() []string {
	return []string{}
}
