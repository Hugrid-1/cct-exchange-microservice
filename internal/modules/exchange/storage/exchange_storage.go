package storage

import (
	"context"
	"github.com/Masterminds/squirrel"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/internal/db/adapter"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/internal/infrastructure/cache"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/internal/infrastructure/db/scanner"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/public/models"
)

const (
	exchangeCacheKey     = "exchange:%d"
	exchangeCacheTTL     = 15
	exchangeCacheTimeout = 50
)

type ExchangeStorage struct {
	adapter *adapter.SQLAdapter
	cache   cache.Cache
}

func NewExchangeStorage(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *ExchangeStorage {
	return &ExchangeStorage{adapter: sqlAdapter, cache: cache}
}

func (e ExchangeStorage) CreatePair(ctx context.Context, dto ExchangeDTO) (int64, error) {
	id, err := e.adapter.Create(ctx, &dto)
	return int64(id), err
}

func (e ExchangeStorage) UpdatePair(ctx context.Context, dto ExchangeDTO) error {
	err := e.adapter.Update(ctx, &dto, adapter.Condition{Equal: squirrel.Eq{"id": dto.ID}}, scanner.Update)
	return err
}

func (e ExchangeStorage) CreateMaxPair(ctx context.Context, dto ExchangeMaxDTO) error {
	_, err := e.adapter.Create(ctx, &dto)
	return err
}

func (e ExchangeStorage) CreateMinPair(ctx context.Context, dto ExchangeMinDTO) error {
	_, err := e.adapter.Create(ctx, &dto)
	return err
}

func (e ExchangeStorage) UpdateMaxPair(ctx context.Context, dto ExchangeMaxDTO) error {
	err := e.adapter.Update(ctx, &dto, adapter.Condition{Equal: squirrel.Eq{"pair_name": dto.PairName}}, scanner.Update)
	return err
}

func (e ExchangeStorage) UpdateMinPair(ctx context.Context, dto ExchangeMinDTO) error {
	err := e.adapter.Update(ctx, &dto, adapter.Condition{Equal: squirrel.Eq{"pair_name": dto.PairName}}, scanner.Update)
	return err
}

func (e ExchangeStorage) GetPairs(ctx context.Context) ([]ExchangeDTO, error) {
	var pairsList []ExchangeDTO
	err := e.adapter.List(ctx, &pairsList, "pairs_history", adapter.Condition{})
	if err != nil {
		return nil, err
	}
	return pairsList, nil
}

func (e ExchangeStorage) GetPairsByIDs(ctx context.Context, ids []int64) ([]ExchangeDTO, error) {
	var pairsList []ExchangeDTO
	err := e.adapter.List(ctx, &pairsList, "pairs_history", adapter.Condition{Equal: map[string]interface{}{"id": ids}})
	if err != nil {
		return nil, err
	}
	return pairsList, nil
}

func (e ExchangeStorage) GetMinPairs(ctx context.Context) (models.Exchange, error) {
	var pairsList []ExchangeMinDTO
	err := e.adapter.List(ctx, &pairsList, "min_pairs", adapter.Condition{})
	if err != nil {
		return nil, err
	}
	ids := make([]int64, 0, len(pairsList))
	for i := range pairsList {
		ids = append(ids, pairsList[i].ExchangeID)
	}

	pairsDataList, err := e.GetPairsByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}
	var result models.Exchange
	result = make(models.Exchange)

	for _, pair := range pairsDataList {
		result[pair.PairName] = models.ExchangeStruct{
			BuyPrice:  pair.BuyPrice,
			SellPrice: pair.SellPrice,
			LastTrade: pair.LastTrade,
			High:      pair.High,
			Low:       pair.Low,
			Avg:       pair.Avg,
			Vol:       pair.Vol,
			VolCurr:   pair.VolCurr,
			Updated:   int(pair.Updated),
		}
	}
	return result, nil
}

func (e ExchangeStorage) GetMaxPairs(ctx context.Context) (models.Exchange, error) {
	var pairsList []ExchangeMaxDTO
	err := e.adapter.List(ctx, &pairsList, "max_pairs", adapter.Condition{})
	if err != nil {
		return nil, err
	}
	ids := make([]int64, 0, len(pairsList))
	for i := range pairsList {
		ids = append(ids, pairsList[i].ExchangeID)
	}

	pairsDataList, err := e.GetPairsByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}
	var result models.Exchange
	result = make(models.Exchange)
	for _, pair := range pairsDataList {
		result[pair.PairName] = models.ExchangeStruct{
			BuyPrice:  pair.BuyPrice,
			SellPrice: pair.SellPrice,
			LastTrade: pair.LastTrade,
			High:      pair.High,
			Low:       pair.Low,
			Avg:       pair.Avg,
			Vol:       pair.Vol,
			VolCurr:   pair.VolCurr,
			Updated:   int(pair.Updated),
		}
	}
	return result, nil
}

func (e ExchangeStorage) GetAvgPairs(ctx context.Context) (models.Exchange, error) {
	var err error
	var result models.Exchange
	result = make(models.Exchange)

	minPairsList, err := e.GetMinPairs(ctx)
	if err != nil {
		return nil, err
	}

	maxPairsList, err := e.GetMaxPairs(ctx)
	if err != nil {
		return nil, err
	}

	for i, pair := range minPairsList {
		result[i] = models.ExchangeStruct{
			BuyPrice:  (minPairsList[i].BuyPrice + maxPairsList[i].BuyPrice) / 2,
			SellPrice: (minPairsList[i].SellPrice + maxPairsList[i].SellPrice) / 2,
			LastTrade: pair.LastTrade,
			High:      pair.High,
			Low:       pair.Low,
			Avg:       pair.Avg,
			Vol:       pair.Vol,
			VolCurr:   pair.VolCurr,
			Updated:   pair.Updated,
		}
	}
	return result, nil
}
