package service

import (
	"context"
	"fmt"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/internal/infrastructure/errors"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/internal/modules/exchange/storage"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/public/messages"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/public/models"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/rabbitmq"
	"go.uber.org/zap"
)

const updateDifference = 100

type ExchangeStorager interface {
	CreatePair(ctx context.Context, dto storage.ExchangeDTO) (int64, error) // create new pair
	UpdatePair(ctx context.Context, dto storage.ExchangeDTO) error          // update pair
	CreateMaxPair(ctx context.Context, dto storage.ExchangeMaxDTO) error    // create new max pair note
	CreateMinPair(ctx context.Context, dto storage.ExchangeMinDTO) error    // create new min pair note
	UpdateMaxPair(ctx context.Context, dto storage.ExchangeMaxDTO) error    // update max pair note
	UpdateMinPair(ctx context.Context, dto storage.ExchangeMinDTO) error    // update min pair note

	GetPairs(ctx context.Context) ([]storage.ExchangeDTO, error)                   // get pairs list/history
	GetPairsByIDs(ctx context.Context, ids []int64) ([]storage.ExchangeDTO, error) // get pairs by ids
	GetMinPairs(ctx context.Context) (models.Exchange, error)                      // get min pairs information
	GetMaxPairs(ctx context.Context) (models.Exchange, error)                      // get max pairs information
	GetAvgPairs(ctx context.Context) (models.Exchange, error)                      // get avg pairs list
}

type ExchangeService struct {
	storage    ExchangeStorager
	exmoAPI    *ExmoAPI
	logger     *zap.Logger
	rabbitMQ   rabbitmq.RabbitMQBroker
	lastUpdate models.Exchange
}

func NewExchangeService(storage ExchangeStorager, logger *zap.Logger, rabbitMQ rabbitmq.RabbitMQBroker) *ExchangeService {
	exmoAPI := &ExmoAPI{}
	return &ExchangeService{storage: storage, logger: logger, exmoAPI: exmoAPI, rabbitMQ: rabbitMQ}
}

func (e ExchangeService) GetPairs(ctx context.Context) messages.ExchangeHistoryOut {
	pairsList, err := e.storage.GetPairs(ctx)
	if err != nil {
		return messages.ExchangeHistoryOut{
			Success:   false,
			ErrorCode: errors.ExchangeServiceHistoryErr,
			Data:      nil,
		}
	}
	var pairs models.Exchange
	for _, pair := range pairsList {
		pairs[pair.PairName] = models.ExchangeStruct{
			BuyPrice:  pair.BuyPrice,
			SellPrice: pair.SellPrice,
			LastTrade: pair.LastTrade,
			High:      pair.High,
			Low:       pair.High,
			Avg:       pair.Avg,
			Vol:       pair.Vol,
			VolCurr:   pair.VolCurr,
			Updated:   int(pair.Updated),
		}
	}
	var pairsHistory map[string][]models.ExchangeStruct
	for pairName, pair := range pairs {
		pairsHistory[pairName] = append(pairsHistory[pairName], pair)
	}
	return messages.ExchangeHistoryOut{
		Success:   true,
		ErrorCode: 0,
		Data:      pairsHistory,
	}
}

func (e ExchangeService) GetMinPairs(ctx context.Context) messages.ExchangeOut {
	pairs, err := e.storage.GetMinPairs(ctx)
	if err != nil {
		return messages.ExchangeOut{
			Success:   false,
			ErrorCode: errors.ExchangeServiceMinErr,
			Data:      nil,
		}
	}
	return messages.ExchangeOut{
		Success:   true,
		ErrorCode: 0,
		Data:      pairs,
	}
}

func (e ExchangeService) GetMaxPairs(ctx context.Context) messages.ExchangeOut {
	pairs, err := e.storage.GetMinPairs(ctx)
	if err != nil {
		return messages.ExchangeOut{
			Success:   false,
			ErrorCode: errors.ExchangeServiceMaxErr,
			Data:      nil,
		}
	}
	return messages.ExchangeOut{
		Success:   true,
		ErrorCode: 0,
		Data:      pairs,
	}
}

func (e ExchangeService) GetAveragePairs(ctx context.Context) messages.ExchangeOut {
	pairs, err := e.storage.GetAvgPairs(ctx)
	if err != nil {
		return messages.ExchangeOut{
			Success:   false,
			ErrorCode: errors.ExchangeServiceAverageErr,
			Data:      nil,
		}
	}
	return messages.ExchangeOut{
		Success:   true,
		ErrorCode: 0,
		Data:      pairs,
	}
}

func (e ExchangeService) GetTicker(ctx context.Context) messages.ExchangeOut {
	pairsList, err := e.exmoAPI.Ticker()
	if err != nil {
		return messages.ExchangeOut{
			Success:   false,
			ErrorCode: errors.ExchangeServiceTickerErr}
	}
	err = e.create(ctx, pairsList)
	if err != nil {
		return messages.ExchangeOut{
			Success:   false,
			ErrorCode: errors.ExchangeServiceCreateErr,
			Data:      pairsList,
		}
	}
	return messages.ExchangeOut{
		Success:   true,
		ErrorCode: 0,
		Data:      pairsList,
	}
}

func (e ExchangeService) GetPairNames(ctx context.Context) messages.ExchangePairsListOut {
	pairsNameList, err := e.exmoAPI.Pairs()
	if err != nil {
		return messages.ExchangePairsListOut{
			Success:   false,
			ErrorCode: errors.ExchangeServicePairNamesErr}
	}
	return messages.ExchangePairsListOut{
		Success:   true,
		ErrorCode: 0,
		Data:      pairsNameList,
	}
}

func (e ExchangeService) create(ctx context.Context, exchange models.Exchange) error {

	var pairsDTOList []storage.ExchangeDTO
	for pairName, pair := range exchange {
		pairsDTOList = append(pairsDTOList, storage.ExchangeDTO{
			ID:        0,
			PairName:  pairName,
			BuyPrice:  pair.BuyPrice,
			SellPrice: pair.SellPrice,
			LastTrade: pair.LastTrade,
			High:      pair.High,
			Low:       pair.Low,
			Avg:       pair.Avg,
			Vol:       pair.Vol,
			VolCurr:   pair.VolCurr,
			Updated:   int64(pair.Updated),
		})
	}

	minList, err := e.storage.GetMinPairs(ctx)
	if err != nil {
		return err
	}
	maxList, err := e.storage.GetMaxPairs(ctx)
	if err != nil {
		return err
	}
	for _, pair := range pairsDTOList {
		if int(pair.Updated) == e.lastUpdate[pair.PairName].Updated {
			continue
		}
		exchangeID, err := e.storage.CreatePair(ctx, pair)
		if err != nil {
			return err
		}
		if pair.BuyPrice > (e.lastUpdate[pair.PairName].BuyPrice+updateDifference) || pair.BuyPrice < (e.lastUpdate[pair.PairName].BuyPrice-updateDifference) {
			message := fmt.Sprintf("New buy price %s :%s", pair.PairName, pair.BuyPrice)
			err = e.rabbitMQ.SendMessage([]byte(message))
			e.logger.Error("rabbitmq send message error")
		}

		if _, ok := minList[pair.PairName]; !ok {
			err = e.storage.CreateMinPair(ctx, storage.ExchangeMinDTO{
				PairName:   pair.PairName,
				ExchangeID: exchangeID,
			})
			if err != nil {
				return err
			}

		} else {
			err = e.storage.UpdateMinPair(ctx, storage.ExchangeMinDTO{
				PairName:   pair.PairName,
				ExchangeID: exchangeID,
			})
			if err != nil {
				return err
			}
		}

		if _, ok := maxList[pair.PairName]; !ok {
			err = e.storage.CreateMaxPair(ctx, storage.ExchangeMaxDTO{
				PairName:   pair.PairName,
				ExchangeID: exchangeID,
			})
			if err != nil {
				return err
			}
		} else {
			err = e.storage.UpdateMaxPair(ctx, storage.ExchangeMaxDTO{
				PairName:   pair.PairName,
				ExchangeID: exchangeID,
			})
			if err != nil {
				return err
			}
		}

	}
	e.setLastUpdate(exchange)
	return nil
}
func (e ExchangeService) setLastUpdate(exchange models.Exchange) {
	e.lastUpdate = exchange
}
