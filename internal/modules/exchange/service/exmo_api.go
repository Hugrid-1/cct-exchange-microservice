package service

import (
	"encoding/json"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/public/messages"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/public/models"
	"io"
	"net/http"
)

const (
	tickerURL             = "https://api.exmo.com/v1.1/ticker"
	currencyPairsNamesURL = "https://api.exmo.com/v1.1/currency/list/extended"
	contentType           = "application/x-www-form-urlencoded"
)

type ExmoAPI struct {
}

func (e ExmoAPI) Ticker() (models.Exchange, error) {
	var result models.Exchange
	response, err := http.Post(tickerURL, contentType, nil)
	if err != nil {
		return nil, err
	}
	respBody, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(respBody, &result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (e ExmoAPI) Pairs() ([]messages.Pair, error) {
	var result []messages.Pair
	resp, err := http.Get(currencyPairsNamesURL)
	if err != nil {
		return nil, err
	}
	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(respBody, &result)
	if err != nil {
		return nil, err
	}
	return result, nil
}
