package controller

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/internal/infrastructure/component"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/internal/infrastructure/responder"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/public/interfaces"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/public/messages"
	"net/http"
)

type Exchange struct {
	service interfaces.Exchanger
	responder.Responder
	godecoder.Decoder
}

func NewExchange(service interfaces.Exchanger, components *component.Components) *Exchange {
	return &Exchange{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (e Exchange) GetHistory(writer http.ResponseWriter, request *http.Request) {
	pairs := e.service.GetPairs(request.Context())
	e.OutputJSON(writer, messages.ExchangeHistoryOut{
		Success:   pairs.Success,
		ErrorCode: pairs.ErrorCode,
		Data:      pairs.Data,
	})
}

func (e Exchange) GetAvgPairs(writer http.ResponseWriter, request *http.Request) {
	pairs := e.service.GetAveragePairs(request.Context())
	e.OutputJSON(writer, messages.ExchangeOut{
		Success:   pairs.Success,
		ErrorCode: pairs.ErrorCode,
		Data:      pairs.Data,
	})
}

func (e Exchange) GetMinPairs(writer http.ResponseWriter, request *http.Request) {
	pairs := e.service.GetMinPairs(request.Context())
	e.OutputJSON(writer, messages.ExchangeOut{
		Success:   pairs.Success,
		ErrorCode: pairs.ErrorCode,
		Data:      pairs.Data,
	})
}

func (e Exchange) GetMaxPairs(writer http.ResponseWriter, request *http.Request) {
	pairs := e.service.GetMaxPairs(request.Context())
	e.OutputJSON(writer, messages.ExchangeOut{
		Success:   pairs.Success,
		ErrorCode: pairs.ErrorCode,
		Data:      pairs.Data,
	})
}
func (e Exchange) GetTicker(writer http.ResponseWriter, request *http.Request) {
	pairs := e.service.GetTicker(request.Context())
	e.OutputJSON(writer, messages.ExchangeOut{
		Success:   pairs.Success,
		ErrorCode: pairs.ErrorCode,
		Data:      pairs.Data,
	})
}

func (e Exchange) GetPairsNameList(writer http.ResponseWriter, request *http.Request) {
	pairs := e.service.GetPairNames(request.Context())
	e.OutputJSON(writer, messages.ExchangePairsListOut{
		Success:   pairs.Success,
		ErrorCode: pairs.ErrorCode,
		Data:      pairs.Data,
	})
}
