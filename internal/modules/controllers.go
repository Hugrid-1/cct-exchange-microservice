package modules

import (
	"gitlab.com/Hugrid-1/cct-exchange-microservice/internal/infrastructure/component"
	excontroller "gitlab.com/Hugrid-1/cct-exchange-microservice/internal/modules/exchange/controller"
	"net/http"
)

type Exchanger interface {
	GetHistory(http.ResponseWriter, *http.Request)
	GetAvgPairs(http.ResponseWriter, *http.Request)
	GetMinPairs(http.ResponseWriter, *http.Request)
	GetMaxPairs(http.ResponseWriter, *http.Request)
	GetTicker(http.ResponseWriter, *http.Request)
	GetPairsNameList(http.ResponseWriter, *http.Request)
}

type Controllers struct {
	Exchange Exchanger
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	exchange := excontroller.NewExchange(services.Exchange, components)

	return &Controllers{
		Exchange: exchange,
	}
}
