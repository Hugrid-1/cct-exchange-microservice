package storages

import (
	"gitlab.com/Hugrid-1/cct-exchange-microservice/internal/db/adapter"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/internal/infrastructure/cache"
	exstorage "gitlab.com/Hugrid-1/cct-exchange-microservice/internal/modules/exchange/storage"
)

type Storages struct {
	Exchange *exstorage.ExchangeStorage
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		Exchange: exstorage.NewExchangeStorage(sqlAdapter, cache),
	}
}
