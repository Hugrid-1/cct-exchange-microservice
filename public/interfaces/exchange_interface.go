package interfaces

import (
	"context"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/public/messages"
)

//go:generate easytags $GOFILE

type Exchanger interface {
	GetPairs(ctx context.Context) messages.ExchangeHistoryOut
	GetMinPairs(ctx context.Context) messages.ExchangeOut
	GetMaxPairs(ctx context.Context) messages.ExchangeOut
	GetAveragePairs(ctx context.Context) messages.ExchangeOut
	GetTicker(ctx context.Context) messages.ExchangeOut
	GetPairNames(ctx context.Context) messages.ExchangePairsListOut
}
