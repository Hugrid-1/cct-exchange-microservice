package models

// хеш-мапа для хранения названия пары и данных по паре
type Exchange map[string]ExchangeStruct

// структура для хранения данных пары
type ExchangeStruct struct {
	BuyPrice  float64 `json:"buy_price,string"`
	SellPrice float64 `json:"sell_price,string"`
	LastTrade float64 `json:"last_trade,string"`
	High      float64 `json:"high,string"`
	Low       float64 `json:"low,string"`
	Avg       float64 `json:"avg,string"`
	Vol       float64 `json:"vol,string"`
	VolCurr   float64 `json:"vol_curr,string"`
	Updated   int     `json:"updated"`
}
