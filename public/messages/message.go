package messages

import "gitlab.com/Hugrid-1/cct-exchange-microservice/public/models"

//go:generate easytags $GOFILE

type ExchangeCreateIn struct {
	Data models.Exchange `json:"data"`
}

type ExchangeOut struct {
	Success   bool            `json:"success"`
	ErrorCode int             `json:"error_code"`
	Data      models.Exchange `json:"data"`
}

type ExchangeHistoryOut struct {
	Success   bool                               `json:"success"`
	ErrorCode int                                `json:"error_code"`
	Data      map[string][]models.ExchangeStruct `json:"data"`
}

type ExchangePairsListOut struct {
	Success   bool   `json:"success"`
	ErrorCode int    `json:"error_code"`
	Data      []Pair `json:"data"`
}

type Pair struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}
