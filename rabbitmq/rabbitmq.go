package rabbitmq

import (
	"context"
	"fmt"
	rbbmq "github.com/rabbitmq/amqp091-go"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/config"
	"go.uber.org/zap"
	"time"
)

type RabbitMQBroker struct {
	conf   config.RabbitMQ
	logger *zap.Logger
	conn   *rbbmq.Connection
	ch     *rbbmq.Channel
}

func NewRabbitMQBroker(conf config.RabbitMQ, logger *zap.Logger) *RabbitMQBroker {
	return &RabbitMQBroker{conf: conf, logger: logger}
}

func (rb RabbitMQBroker) SendMessage(message []byte) error {
	var err error

	connStr := fmt.Sprintf("amqp://%s:%s@%s:%s/", rb.conf.User, rb.conf.Password, rb.conf.Host, rb.conf.Port)

	rb.conn, err = rbbmq.Dial(connStr)
	if err != nil {
		rb.logger.Error("rabbitmq: failed init", zap.Error(err))
		return err
	}

	defer func() {
		_ = rb.conn.Close()
	}()

	rb.ch, err = rb.conn.Channel()
	if err != nil {
		rb.logger.Error("rabbitmq: chanel error", zap.Error(err))
	}
	defer func() {
		_ = rb.ch.Close()
	}()

	q, err := rb.ch.QueueDeclare(
		"exchange",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		rb.logger.Error("rabbitmq: queue declare error", zap.Error(err))
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err = rb.ch.PublishWithContext(ctx,
		"",
		q.Name,
		false,
		false,
		rbbmq.Publishing{
			ContentType: "text/plain",
			Body:        message,
		})
	if err != nil {
		rb.logger.Error("rabbitmq: publish error", zap.Error(err))
	}
	return nil
}
