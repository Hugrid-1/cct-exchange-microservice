package jrpc

import (
	"context"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/internal/infrastructure/errors"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/public/messages"
	"net/rpc"
)

type ExchangeJRPCClient struct {
	client *rpc.Client
}

func NewExchangeJRPCClient(serverAddress string) (*ExchangeJRPCClient, error) {
	client, err := rpc.Dial("tcp", serverAddress)
	if err != nil {
		return nil, err
	}

	return &ExchangeJRPCClient{client: client}, nil
}

func (e ExchangeJRPCClient) GetPairs(ctx context.Context) messages.ExchangeHistoryOut {
	var out messages.ExchangeHistoryOut
	err := e.client.Call("ExchangeJRPCServer.GetPairs", nil, &out)
	if err != nil {
		return messages.ExchangeHistoryOut{ErrorCode: errors.ExchangeServiceHistoryErr}
	}
	return out
}

func (e ExchangeJRPCClient) GetMinPairs(ctx context.Context) messages.ExchangeOut {
	var out messages.ExchangeOut
	err := e.client.Call("ExchangeJRPCServer.GetMinPairs", nil, &out)
	if err != nil {
		return messages.ExchangeOut{ErrorCode: errors.ExchangeServiceMinErr}
	}
	return out
}

func (e ExchangeJRPCClient) GetMaxPairs(ctx context.Context) messages.ExchangeOut {
	var out messages.ExchangeOut
	err := e.client.Call("ExchangeJRPCServer.GetMaxPairs", nil, &out)
	if err != nil {
		return messages.ExchangeOut{ErrorCode: errors.ExchangeServiceMaxErr}
	}
	return out
}

func (e ExchangeJRPCClient) GetAveragePairs(ctx context.Context) messages.ExchangeOut {
	var out messages.ExchangeOut
	err := e.client.Call("ExchangeJRPCServer.GetAveragePairs", nil, &out)
	if err != nil {
		return messages.ExchangeOut{ErrorCode: errors.ExchangeServiceHistoryErr}
	}
	return out
}

func (e ExchangeJRPCClient) GetTicker(ctx context.Context) messages.ExchangeOut {
	var out messages.ExchangeOut
	err := e.client.Call("ExchangeJRPCServer.GetTicker", nil, &out)
	if err != nil {
		return messages.ExchangeOut{ErrorCode: errors.ExchangeServiceTickerErr}
	}
	return out
}

func (e ExchangeJRPCClient) GetPairNames(ctx context.Context) messages.ExchangePairsListOut {
	var out messages.ExchangePairsListOut
	err := e.client.Call("ExchangeJRPCServer.GetPairNames", nil, &out)
	if err != nil {
		return messages.ExchangePairsListOut{ErrorCode: errors.ExchangeServicePairNamesErr}
	}
	return out
}
