package jrpc

import (
	"context"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/public/interfaces"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/public/messages"
)

type ExchangeJRPCServer struct {
	exchangeService interfaces.Exchanger
}

func NewExchangeJRPCServer(exchangeService interfaces.Exchanger) *ExchangeJRPCServer {
	return &ExchangeJRPCServer{exchangeService: exchangeService}
}

func (e ExchangeJRPCServer) GetPairs(out *messages.ExchangeHistoryOut) error {
	*out = e.exchangeService.GetPairs(context.Background())
	return nil
}

func (e ExchangeJRPCServer) GetMinPairs(out *messages.ExchangeOut) error {
	*out = e.exchangeService.GetMinPairs(context.Background())
	return nil
}

func (e ExchangeJRPCServer) GetMaxPairs(out *messages.ExchangeOut) error {
	*out = e.exchangeService.GetMaxPairs(context.Background())
	return nil
}

func (e ExchangeJRPCServer) GetAveragePairs(out *messages.ExchangeOut) error {
	*out = e.exchangeService.GetAveragePairs(context.Background())
	return nil
}

func (e ExchangeJRPCServer) GetTicker(out *messages.ExchangeOut) error {
	*out = e.exchangeService.GetTicker(context.Background())
	return nil
}

func (e ExchangeJRPCServer) GetPairNames(out *messages.ExchangePairsListOut) error {
	*out = e.exchangeService.GetPairNames(context.Background())
	return nil
}
