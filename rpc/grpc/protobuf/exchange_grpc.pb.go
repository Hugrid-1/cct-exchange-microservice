// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.12.4
// source: exchange.proto

package protobuf

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// ExchangeClient is the client API for Exchange service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type ExchangeClient interface {
	GetPairs(ctx context.Context, in *ExchangeRequest, opts ...grpc.CallOption) (*ExchangeHistoryResponse, error)
	GetMinPairs(ctx context.Context, in *ExchangeRequest, opts ...grpc.CallOption) (*ExchangeResponse, error)
	GetMaxPairs(ctx context.Context, in *ExchangeRequest, opts ...grpc.CallOption) (*ExchangeResponse, error)
	GetAveragePairs(ctx context.Context, in *ExchangeRequest, opts ...grpc.CallOption) (*ExchangeResponse, error)
	GetTicker(ctx context.Context, in *ExchangeRequest, opts ...grpc.CallOption) (*ExchangeResponse, error)
	GetPairNames(ctx context.Context, in *ExchangeRequest, opts ...grpc.CallOption) (*ExchangePairNamesResponse, error)
}

type exchangeClient struct {
	cc grpc.ClientConnInterface
}

func NewExchangeClient(cc grpc.ClientConnInterface) ExchangeClient {
	return &exchangeClient{cc}
}

func (c *exchangeClient) GetPairs(ctx context.Context, in *ExchangeRequest, opts ...grpc.CallOption) (*ExchangeHistoryResponse, error) {
	out := new(ExchangeHistoryResponse)
	err := c.cc.Invoke(ctx, "/protobuf.Exchange/GetPairs", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *exchangeClient) GetMinPairs(ctx context.Context, in *ExchangeRequest, opts ...grpc.CallOption) (*ExchangeResponse, error) {
	out := new(ExchangeResponse)
	err := c.cc.Invoke(ctx, "/protobuf.Exchange/GetMinPairs", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *exchangeClient) GetMaxPairs(ctx context.Context, in *ExchangeRequest, opts ...grpc.CallOption) (*ExchangeResponse, error) {
	out := new(ExchangeResponse)
	err := c.cc.Invoke(ctx, "/protobuf.Exchange/GetMaxPairs", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *exchangeClient) GetAveragePairs(ctx context.Context, in *ExchangeRequest, opts ...grpc.CallOption) (*ExchangeResponse, error) {
	out := new(ExchangeResponse)
	err := c.cc.Invoke(ctx, "/protobuf.Exchange/GetAveragePairs", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *exchangeClient) GetTicker(ctx context.Context, in *ExchangeRequest, opts ...grpc.CallOption) (*ExchangeResponse, error) {
	out := new(ExchangeResponse)
	err := c.cc.Invoke(ctx, "/protobuf.Exchange/GetTicker", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *exchangeClient) GetPairNames(ctx context.Context, in *ExchangeRequest, opts ...grpc.CallOption) (*ExchangePairNamesResponse, error) {
	out := new(ExchangePairNamesResponse)
	err := c.cc.Invoke(ctx, "/protobuf.Exchange/GetPairNames", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ExchangeServer is the server API for Exchange service.
// All implementations must embed UnimplementedExchangeServer
// for forward compatibility
type ExchangeServer interface {
	GetPairs(context.Context, *ExchangeRequest) (*ExchangeHistoryResponse, error)
	GetMinPairs(context.Context, *ExchangeRequest) (*ExchangeResponse, error)
	GetMaxPairs(context.Context, *ExchangeRequest) (*ExchangeResponse, error)
	GetAveragePairs(context.Context, *ExchangeRequest) (*ExchangeResponse, error)
	GetTicker(context.Context, *ExchangeRequest) (*ExchangeResponse, error)
	GetPairNames(context.Context, *ExchangeRequest) (*ExchangePairNamesResponse, error)
	mustEmbedUnimplementedExchangeServer()
}

// UnimplementedExchangeServer must be embedded to have forward compatible implementations.
type UnimplementedExchangeServer struct {
}

func (UnimplementedExchangeServer) GetPairs(context.Context, *ExchangeRequest) (*ExchangeHistoryResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetPairs not implemented")
}
func (UnimplementedExchangeServer) GetMinPairs(context.Context, *ExchangeRequest) (*ExchangeResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetMinPairs not implemented")
}
func (UnimplementedExchangeServer) GetMaxPairs(context.Context, *ExchangeRequest) (*ExchangeResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetMaxPairs not implemented")
}
func (UnimplementedExchangeServer) GetAveragePairs(context.Context, *ExchangeRequest) (*ExchangeResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAveragePairs not implemented")
}
func (UnimplementedExchangeServer) GetTicker(context.Context, *ExchangeRequest) (*ExchangeResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetTicker not implemented")
}
func (UnimplementedExchangeServer) GetPairNames(context.Context, *ExchangeRequest) (*ExchangePairNamesResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetPairNames not implemented")
}
func (UnimplementedExchangeServer) mustEmbedUnimplementedExchangeServer() {}

// UnsafeExchangeServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ExchangeServer will
// result in compilation errors.
type UnsafeExchangeServer interface {
	mustEmbedUnimplementedExchangeServer()
}

func RegisterExchangeServer(s grpc.ServiceRegistrar, srv ExchangeServer) {
	s.RegisterService(&Exchange_ServiceDesc, srv)
}

func _Exchange_GetPairs_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ExchangeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ExchangeServer).GetPairs(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protobuf.Exchange/GetPairs",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ExchangeServer).GetPairs(ctx, req.(*ExchangeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Exchange_GetMinPairs_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ExchangeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ExchangeServer).GetMinPairs(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protobuf.Exchange/GetMinPairs",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ExchangeServer).GetMinPairs(ctx, req.(*ExchangeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Exchange_GetMaxPairs_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ExchangeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ExchangeServer).GetMaxPairs(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protobuf.Exchange/GetMaxPairs",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ExchangeServer).GetMaxPairs(ctx, req.(*ExchangeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Exchange_GetAveragePairs_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ExchangeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ExchangeServer).GetAveragePairs(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protobuf.Exchange/GetAveragePairs",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ExchangeServer).GetAveragePairs(ctx, req.(*ExchangeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Exchange_GetTicker_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ExchangeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ExchangeServer).GetTicker(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protobuf.Exchange/GetTicker",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ExchangeServer).GetTicker(ctx, req.(*ExchangeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Exchange_GetPairNames_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ExchangeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ExchangeServer).GetPairNames(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protobuf.Exchange/GetPairNames",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ExchangeServer).GetPairNames(ctx, req.(*ExchangeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// Exchange_ServiceDesc is the grpc.ServiceDesc for Exchange service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Exchange_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "protobuf.Exchange",
	HandlerType: (*ExchangeServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetPairs",
			Handler:    _Exchange_GetPairs_Handler,
		},
		{
			MethodName: "GetMinPairs",
			Handler:    _Exchange_GetMinPairs_Handler,
		},
		{
			MethodName: "GetMaxPairs",
			Handler:    _Exchange_GetMaxPairs_Handler,
		},
		{
			MethodName: "GetAveragePairs",
			Handler:    _Exchange_GetAveragePairs_Handler,
		},
		{
			MethodName: "GetTicker",
			Handler:    _Exchange_GetTicker_Handler,
		},
		{
			MethodName: "GetPairNames",
			Handler:    _Exchange_GetPairNames_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "exchange.proto",
}
