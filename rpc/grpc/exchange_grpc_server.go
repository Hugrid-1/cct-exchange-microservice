package grpc

import (
	"context"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/public/interfaces"
	pb "gitlab.com/Hugrid-1/cct-exchange-microservice/rpc/grpc/protobuf"
)

type ExchangeGRPCServer struct {
	exchangeService interfaces.Exchanger
	pb.UnimplementedExchangeServer
}

func NewExchangeGRPCServer(exchangeService interfaces.Exchanger) *ExchangeGRPCServer {

	return &ExchangeGRPCServer{exchangeService: exchangeService}
}
func (e ExchangeGRPCServer) GetPairs(ctx context.Context, in *pb.ExchangeRequest) (*pb.ExchangeHistoryResponse, error) {
	out := e.exchangeService.GetPairs(ctx)
	var result map[string]*pb.ExchangeList
	for pairName, pairData := range out.Data {
		for _, pairStruct := range pairData {
			result[pairName].List = append(result[pairName].List, &pb.ExchangeStruct{
				BuyPrice:  pairStruct.BuyPrice,
				SellPrice: pairStruct.SellPrice,
				LastTrade: pairStruct.LastTrade,
				High:      pairStruct.High,
				Low:       pairStruct.Low,
				Avg:       pairStruct.Avg,
				Vol:       pairStruct.Vol,
				VolCurr:   pairStruct.VolCurr,
				Updated:   int64(pairStruct.Updated),
			})
		}

	}
	return &pb.ExchangeHistoryResponse{
		Success:   true,
		ErrorCode: 0,
		Data:      result,
	}, nil
}

func (e ExchangeGRPCServer) GetMinPairs(ctx context.Context, in *pb.ExchangeRequest) (*pb.ExchangeResponse, error) {
	out := e.exchangeService.GetMinPairs(ctx)
	var result map[string]*pb.ExchangeStruct
	for pairName, pairData := range out.Data {
		result[pairName] = &pb.ExchangeStruct{
			BuyPrice:  pairData.BuyPrice,
			SellPrice: pairData.SellPrice,
			LastTrade: pairData.LastTrade,
			High:      pairData.High,
			Low:       pairData.Low,
			Avg:       pairData.Avg,
			Vol:       pairData.Vol,
			VolCurr:   pairData.VolCurr,
			Updated:   int64(pairData.Updated),
		}
	}
	return &pb.ExchangeResponse{
		Success:   true,
		ErrorCode: 0,
		Data:      result,
	}, nil
}

func (e ExchangeGRPCServer) GetMaxPairs(ctx context.Context, in *pb.ExchangeRequest) (*pb.ExchangeResponse, error) {
	out := e.exchangeService.GetMaxPairs(ctx)
	var result map[string]*pb.ExchangeStruct
	for pairName, pairData := range out.Data {
		result[pairName] = &pb.ExchangeStruct{
			BuyPrice:  pairData.BuyPrice,
			SellPrice: pairData.SellPrice,
			LastTrade: pairData.LastTrade,
			High:      pairData.High,
			Low:       pairData.Low,
			Avg:       pairData.Avg,
			Vol:       pairData.Vol,
			VolCurr:   pairData.VolCurr,
			Updated:   int64(pairData.Updated),
		}
	}
	return &pb.ExchangeResponse{
		Success:   true,
		ErrorCode: 0,
		Data:      result,
	}, nil
}

func (e ExchangeGRPCServer) GetAveragePairs(ctx context.Context, in *pb.ExchangeRequest) (*pb.ExchangeResponse, error) {
	out := e.exchangeService.GetAveragePairs(ctx)
	var result map[string]*pb.ExchangeStruct
	for pairName, pairData := range out.Data {
		result[pairName] = &pb.ExchangeStruct{
			BuyPrice:  pairData.BuyPrice,
			SellPrice: pairData.SellPrice,
			LastTrade: pairData.LastTrade,
			High:      pairData.High,
			Low:       pairData.Low,
			Avg:       pairData.Avg,
			Vol:       pairData.Vol,
			VolCurr:   pairData.VolCurr,
			Updated:   int64(pairData.Updated),
		}
	}
	return &pb.ExchangeResponse{
		Success:   true,
		ErrorCode: 0,
		Data:      result,
	}, nil
}

func (e ExchangeGRPCServer) GetTicker(ctx context.Context, in *pb.ExchangeRequest) (*pb.ExchangeResponse, error) {
	out := e.exchangeService.GetTicker(ctx)
	var result map[string]*pb.ExchangeStruct
	for pairName, pairData := range out.Data {
		result[pairName] = &pb.ExchangeStruct{
			BuyPrice:  pairData.BuyPrice,
			SellPrice: pairData.SellPrice,
			LastTrade: pairData.LastTrade,
			High:      pairData.High,
			Low:       pairData.Low,
			Avg:       pairData.Avg,
			Vol:       pairData.Vol,
			VolCurr:   pairData.VolCurr,
			Updated:   int64(pairData.Updated),
		}
	}
	return &pb.ExchangeResponse{
		Success:   true,
		ErrorCode: 0,
		Data:      result,
	}, nil
}

func (e ExchangeGRPCServer) GetPairNames(ctx context.Context, in *pb.ExchangeRequest) (*pb.ExchangePairNamesResponse, error) {
	out := e.exchangeService.GetPairNames(ctx)
	var result []*pb.Pair
	for _, pairData := range out.Data {
		result = append(result, &pb.Pair{
			Name:        pairData.Name,
			Description: pairData.Description,
		})
	}
	return &pb.ExchangePairNamesResponse{
		Success:   true,
		ErrorCode: 0,
		Data:      result,
	}, nil
}
