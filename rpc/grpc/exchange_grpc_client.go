package grpc

import (
	"context"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/internal/infrastructure/errors"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/public/messages"
	"gitlab.com/Hugrid-1/cct-exchange-microservice/public/models"
	pb "gitlab.com/Hugrid-1/cct-exchange-microservice/rpc/grpc/protobuf"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ExchangeGRPCClient struct {
	client pb.ExchangeClient
}

func NewExchangeGRPCClient(address string) (*ExchangeGRPCClient, error) {
	conn, err := grpc.Dial(address, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	client := pb.NewExchangeClient(conn)
	return &ExchangeGRPCClient{
		client: client,
	}, nil
}

func (e ExchangeGRPCClient) GetPairs(ctx context.Context) messages.ExchangeHistoryOut {
	out, err := e.client.GetPairs(ctx, &pb.ExchangeRequest{})
	if err != nil {
		return messages.ExchangeHistoryOut{ErrorCode: errors.ExchangeServiceHistoryErr}
	}
	var result map[string][]models.ExchangeStruct
	for pairName, pairData := range out.Data {
		for _, pairStruct := range pairData.List {
			result[pairName] = append(result[pairName], models.ExchangeStruct{
				BuyPrice:  pairStruct.BuyPrice,
				SellPrice: pairStruct.SellPrice,
				LastTrade: pairStruct.LastTrade,
				High:      pairStruct.High,
				Low:       pairStruct.Low,
				Avg:       pairStruct.Avg,
				Vol:       pairStruct.Vol,
				VolCurr:   pairStruct.VolCurr,
				Updated:   int(pairStruct.Updated),
			})
		}

	}
	return messages.ExchangeHistoryOut{
		Success:   true,
		ErrorCode: 0,
		Data:      result,
	}
}

func (e ExchangeGRPCClient) GetMinPairs(ctx context.Context) messages.ExchangeOut {
	out, err := e.client.GetMinPairs(ctx, &pb.ExchangeRequest{})
	if err != nil {
		return messages.ExchangeOut{ErrorCode: errors.ExchangeServiceMinErr}
	}
	var result models.Exchange
	for pairName, pairData := range out.Data {
		result[pairName] = models.ExchangeStruct{
			BuyPrice:  pairData.BuyPrice,
			SellPrice: pairData.SellPrice,
			LastTrade: pairData.LastTrade,
			High:      pairData.High,
			Low:       pairData.Low,
			Avg:       pairData.Avg,
			Vol:       pairData.Vol,
			VolCurr:   pairData.VolCurr,
			Updated:   int(pairData.Updated),
		}
	}
	return messages.ExchangeOut{
		Success:   true,
		ErrorCode: 0,
		Data:      result,
	}
}

func (e ExchangeGRPCClient) GetMaxPairs(ctx context.Context) messages.ExchangeOut {
	out, err := e.client.GetMaxPairs(ctx, &pb.ExchangeRequest{})
	if err != nil {
		return messages.ExchangeOut{ErrorCode: errors.ExchangeServiceMaxErr}
	}
	var result models.Exchange
	for pairName, pairData := range out.Data {
		result[pairName] = models.ExchangeStruct{
			BuyPrice:  pairData.BuyPrice,
			SellPrice: pairData.SellPrice,
			LastTrade: pairData.LastTrade,
			High:      pairData.High,
			Low:       pairData.Low,
			Avg:       pairData.Avg,
			Vol:       pairData.Vol,
			VolCurr:   pairData.VolCurr,
			Updated:   int(pairData.Updated),
		}
	}
	return messages.ExchangeOut{
		Success:   true,
		ErrorCode: 0,
		Data:      result,
	}
}

func (e ExchangeGRPCClient) GetAveragePairs(ctx context.Context) messages.ExchangeOut {
	out, err := e.client.GetAveragePairs(ctx, &pb.ExchangeRequest{})
	if err != nil {
		return messages.ExchangeOut{ErrorCode: errors.ExchangeServiceAverageErr}
	}
	var result models.Exchange
	for pairName, pairData := range out.Data {
		result[pairName] = models.ExchangeStruct{
			BuyPrice:  pairData.BuyPrice,
			SellPrice: pairData.SellPrice,
			LastTrade: pairData.LastTrade,
			High:      pairData.High,
			Low:       pairData.Low,
			Avg:       pairData.Avg,
			Vol:       pairData.Vol,
			VolCurr:   pairData.VolCurr,
			Updated:   int(pairData.Updated),
		}
	}
	return messages.ExchangeOut{
		Success:   true,
		ErrorCode: 0,
		Data:      result,
	}
}

func (e ExchangeGRPCClient) GetTicker(ctx context.Context) messages.ExchangeOut {
	out, err := e.client.GetTicker(ctx, &pb.ExchangeRequest{})
	if err != nil {
		return messages.ExchangeOut{ErrorCode: errors.ExchangeServiceTickerErr}
	}
	var result models.Exchange
	for pairName, pairData := range out.Data {
		result[pairName] = models.ExchangeStruct{
			BuyPrice:  pairData.BuyPrice,
			SellPrice: pairData.SellPrice,
			LastTrade: pairData.LastTrade,
			High:      pairData.High,
			Low:       pairData.Low,
			Avg:       pairData.Avg,
			Vol:       pairData.Vol,
			VolCurr:   pairData.VolCurr,
			Updated:   int(pairData.Updated),
		}
	}
	return messages.ExchangeOut{
		Success:   true,
		ErrorCode: 0,
		Data:      result,
	}
}

func (e ExchangeGRPCClient) GetPairNames(ctx context.Context) messages.ExchangePairsListOut {
	out, err := e.client.GetPairNames(ctx, &pb.ExchangeRequest{})
	if err != nil {
		return messages.ExchangePairsListOut{ErrorCode: errors.ExchangeServicePairNamesErr}
	}
	var result []messages.Pair
	for _, pairData := range out.Data {
		result = append(result, messages.Pair{
			Name:        pairData.Name,
			Description: pairData.Description,
		})
	}
	return messages.ExchangePairsListOut{
		Success:   true,
		ErrorCode: 0,
		Data:      result,
	}
}
